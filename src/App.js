import logo from "./logo.svg";
import "./App.css";
import { useState, useEffect } from "react";
import jsonData from "./data.json";

function App() {
  const [fileContent, setFileContent] = useState("");
  const [data, setData] = useState({});

  useEffect(() => {
    setData(jsonData); // Assigning the JSON data to the state
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <pre>{fileContent}</pre>
        <pre>{JSON.stringify(data, null, 2)}</pre>
      </header>
    </div>
  );
}

export default App;
